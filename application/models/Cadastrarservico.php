<?php

class Application_Model_Cadastrarservico
{

	public function cadastrarServicoAutonomo($data){

		$dbTableBuscador = new Application_Model_DbTable_Buscador();

		$dados = array(
						'id' 		=> null,
						'id_user' 	=> 20,
						'link' 		=> $data->arrDados->nome_completo,
						'nome' 		=> $data->arrDados->ser_chamado,
						'cidade' 	=> $data->arrDados->cidade,
						'endereco'	=> $data->arrDados->endereco,
						'numero' 	=> (int) $data->arrDados->numero_end,
						'telefone' 	=> $data->arrDados->telefone,
						'servico' 	=> $data->arrDados->profissao,
						'key1' 		=> $data->arrDados->keyword1,
						'key2' 		=> $data->arrDados->keyword2,
						'key3' 		=> $data->arrDados->keyword3,
						'key4' 		=> $data->arrDados->keyword4
		);

		$result = $dbTableBuscador->insert($dados);
		return $result;
	}	

}

