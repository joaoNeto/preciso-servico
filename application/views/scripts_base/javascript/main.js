/* ------------------ VARIAVEIS GLOBAIS ------------------ */
var BASE_URL            = 'http://localhost/precisoservico/public/';
var BASE_URL_VIEW_BASE  = BASE_URL+'application/views/scripts_base/';
var BASE_URL_VIEW       = BASE_URL+'application/views/scripts/';

/* -------------------- SCRIPT DO TOPO ------------------- */
$(document).ready(function(){

    $("#input-buscador").click(function(){
    	$("#resultadoBusca").show();
    	$(".botoes-banner").css("visibility","hidden");
    });

    $("#input-buscador").blur(function(){
  
    	setTimeout(function(){
	        $("#resultadoBusca").hide();
	    	$(".botoes-banner").css("visibility","visible");
		},300);  

    });

    $("#fecharMsgAlert").click(function(){
        $(".mensagemAlerta").fadeOut(100);
    });

    $("#fecharMsgSucess").click(function(){
        $(".mensagemSucess").fadeOut(100);
    });

});

/* ------------------ MENSAGEM DE SUCESSO ------------------ */
function msgSucess(text){
    $(document).ready(function(){
        $(".mensagemSucess").fadeIn(100);
        $("#textoMensagemSucess").text(text);
    });             
}

/* ------------------ MENSAGEM DE ALERTA ------------------ */
function msgAlert(text){
    $(document).ready(function(){
        $(".mensagemAlerta").fadeIn(100);
        $("#textoMensagem").text(text);
    });             
}

/* ------------------ VALIDAÇÃO DE CAMPOS ------------------ */
function validarCampos(arrDados){
    /*
       funcao: validarCampos
       parametros: arrDados
       arrDados[['Qual input será validada','qual o tipo validacao']]
       valores do tipo validacao: nulo, inteiro, string, telefone, dinheiro, email.
    */

    for(var i = 0; i < arrDados.length ; i++){

        var elemento = document.getElementById(arrDados[i][0]);

        switch(arrDados[i][1]) {
            case 'nulo':

                    if( elemento.value == '' || elemento.value == null || elemento.value == undefined ){
                        msgAlert('O campo '+arrDados[i][2]+' deve ser preenchido');
                        elemento.style.border = "1px solid red";
                        setTimeout(function(){elemento.style.border = "0px solid white";}, 2000);
                        return false;
                    }

                break;

            case 'telefone':
                
                break;

            case 'email':
                
                break;

            default:
                console.log('nenhuma acao no validar campos');
        }
    }

    return true;
}


/* --------------------- ANGULAR JS --------------------- */
angular.module('myApp', []).controller('namesCtrl', function($scope,$http) {
    
    $scope.resultadoBusca = [
                            ['link','Restaurante Boi na brasa'  ,'endereço: Rua jiurdano bruno, nº02 | contato: (81) 9999-9999' ,'carne boi vaca galinha nordeste comida'    , 'restaurante'],
                            ['link','joao do gaz'               ,'endereço: Rua jiurdano bruno, nº02 | contato: (81) 9999-9999' ,'gaz'                               , 'gaz'],
                            ['link','paulinho marceneiro'       ,'endereço: Rua samuel borges, nº02 | contato: (81) 9999-9999'  ,'marceneiro'                        , 'marceneiro'],
                            ['link','paulo roberto'             ,'endereço: Rua tres irmãs, nº02 | contato: (81) 9999-9999'     ,'taxista motorista carro transporte', 'taxista'],
                            ['link','Pizzaria de doug'          ,'endereço: Rua jiurdano bruno, nº02 | contato: (81) 9999-9999' ,'pizzaria comida food fast'         , 'pizzaria'],
                            ['link','bruno borges'              ,'endereço: Rua jiurdano bruno, nº02 | contato: (81) 9999-9999' ,'taxista motorista carro transporte', 'taxista'],
                            ['link','luiz claudio'              ,'endereço: Av norte, nº02 | contato: (81) 9999-9999'           ,'gesseiro geseiro gesso concerto'   , 'gesseiro'],
                            ['link','luiz borges'               ,'endereço: Rua do principe, nº02 | contato: (81) 9999-9999'    ,'eletricista fiação tomada'         , 'gesso'],
                            ['link','Churrascaria do bob'       ,'endereço: Rua jiurdano bruno, nº02 | contato: (81) 9999-9999' ,'butijão gas comida'                        , 'churrascaria'],
                            ['link','etevaldo santos'           ,'endereço: Rua giurdano bruno, nº02 | contato: (81) 9999-9999' ,'mecanico carro quebrado pneu'      , 'mecanico'],
                            ['link','felipe dos santos'         ,'endereço: Rua santos drumond, nº02 | contato: (81) 9999-9999' ,'pintor parede cor serviço'         , 'pintor']
                            ];
        
/*        $http.get("crud.php?acao=buscar")
            .success(function(data){
                $scope.data = data;
        })*/

    // INSERINDO USUÁRIO AUTONOMO.
    $scope.cadastrarAutonomo = function(){

        var arrDados =  {
            'nome_completo' : $scope.nome_completo ,
            'ser_chamado'   : $scope.ser_chamado ,
            'profissao'     : $scope.profissao ,
            'descricao'     : $scope.descricao ,
            'estado'        : $scope.estado ,
            'cidade'        : $scope.cidade ,
            'endereco'      : $scope.endereco ,
            'numero_end'    : $scope.numero_end ,
            'telefone'      : $scope.contato ,
            'email'         : $scope.email ,
            'keyword1'      : $scope.palavrachave1 ,
            'keyword2'      : $scope.palavrachave2 ,
            'keyword3'      : $scope.palavrachave3 ,
            'keyword4'      : $scope.palavrachave4
        };

        $http.post(BASE_URL+'cadastrarservico/cadastrarservicoautonomo',{'arrDados': arrDados})
        .success(function(msg){
            msgSucess("Cadastrado com sucesso!!!  :)");
            setTimeout(function(){window.location.href = BASE_URL;}, 2000);
        })
        .error(function(msg){
            msgAlert("Error: Não foi possivel cadastar seus dados. :(");
            $( "#submit" ).prop( "disabled", false );
            $( "#submit" ).css("backgroundColor","#4774a2");
            $( "#submit" ).css("border","2px solid white");
            $( "#submit" ).val("Finalizar");                   
        })
    }


});
