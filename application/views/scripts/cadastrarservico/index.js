$(document).ready(function(){

	$("#tel_contato").mask("(99) 99999 - 9999");
	$("#numero_end").mask("99999");

	$("#mostrarPrimeiraParte").click(function(){

		$(".primeira-etapa-form").fadeIn();
		$(".segunda-etapa-form").fadeOut();
		$(".terceira-etapa-form").fadeOut();			

	});

	$("#mostrarSegundaParte").click(function(){

		// validando os dados do primeira etapa de cadastro
		var arrDados		= [];
			arrDados[0] 	= ["nome_completo"	,"nulo", "nome completo"];
			arrDados[1] 	= ["ser_chamado"	,"nulo", "ser chamado"];
			arrDados[2] 	= ["profissao"		,"nulo", "profissao"];
			arrDados[3] 	= ["descricaoProf"	,"nulo", "descricao"];

		if(validarCampos(arrDados)){
			$(".primeira-etapa-form").fadeOut();
			$(".segunda-etapa-form").fadeIn();
			$(".terceira-etapa-form").fadeOut();
		}

	});

	// botão de voltar na terceira etapa cadastro
	$("#mostrarSegundaParte2").click(function(){

		$(".primeira-etapa-form").fadeOut();
		$(".segunda-etapa-form").fadeIn();
		$(".terceira-etapa-form").fadeOut();

	});

	$("#mostrarTerceiraParte").click(function(){

		// validando os dados da segunda etapa de cadastro
		var arrDados		= [];
			arrDados[0] 	= ["estado"		,"nulo", "estado"];
			arrDados[1] 	= ["cidade"		,"nulo", "cidade"];
			arrDados[2] 	= ["endereco"	,"nulo", "endereco"];
			arrDados[3] 	= ["numero_end"	,"nulo", "numero do endereco"];
			arrDados[4] 	= ["tel_contato","nulo", "telefone de contato"];

		if(validarCampos(arrDados)){
			$(".primeira-etapa-form").fadeOut();
			$(".segunda-etapa-form").fadeOut();
			$(".terceira-etapa-form").fadeIn();
		}
	});

	$("#mostrarDetalhes").click(function(){
		$("#detalhes").fadeIn();
	});

	$("#submit").click(function(){
		$( "#submit" ).prop( "disabled", true );
		$( "#submit" ).css("backgroundColor","darkgrey");
		$( "#submit" ).css("color","white");
		$( "#submit" ).css("border","none");
		$( "#submit" ).val("processando...");
	});

});

