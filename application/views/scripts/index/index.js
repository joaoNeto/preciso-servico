// SERVE PARA SUAVISAR O MOVIMENTO DO SCROLL
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});


$(function() {
  $('#topico-menu-inicio').click(function() {
  
	$('#topo').css('box-shadow', '1px 7px 50px -3px rgba(0,0,0,0.75)');
	$('#area-logotipo').css('background-color','#fff');  	
	$('#logotipo').show();  	
  $('#logotipo-azul').hide();   
  setTimeout(function(){
    $('#buscador').show();   
    $('#botoes-topo').hide();
  },500);   

  });
});


$(function() {
  $('#topico-menu-empresa').click(function() {
  	
	$('#topo').css('box-shadow', '1px 7px 50px -3px rgba(0,0,0,0.75)');
	$('#area-logotipo').css('background-color','#fff');  	
	$('#logotipo').show();  	
  $('#logotipo-azul').hide();
  $('#buscador').hide();   

  setTimeout(function(){
    $('#buscador').hide();   
    $('#botoes-topo').show();
  },500);   

  });
});

$(function() {
  $('#topico-menu-empresa2').click(function() {
  $('#topo').css('box-shadow', '1px 7px 50px -3px rgba(0,0,0,0.75)');
  $('#area-logotipo').css('background-color','#fff');   
  $('#logotipo').show();    
  $('#logotipo-azul').hide();

  setTimeout(function(){
    $('#buscador').hide();   
    $('#botoes-topo').show();
  },500);   

  });
});


$(function() {
  $('#topico-menu-contato').click(function() {

  	setTimeout(function(){
	  	$('#topo').css('box-shadow', '0px 0px 0px #fff');
	  	$('#area-logotipo').css('background-color','#336699');  	
	  	$('#logotipo').hide();  	
	  	$('#logotipo-azul').show();  
      $('#botoes-topo').hide();
      $('#buscador').show();   

	},500); 	

  });
});