<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));


// VARIAVEIS GLOBIAS DA APLICACAO
define('APPLICATION_PATH_VIEW',APPLICATION_PATH.'/views/scripts/');
define('APPLICATION_PATH_MODEL',APPLICATION_PATH.'/models/');
define('APPLICATION_PATH_VIEW_BASE',APPLICATION_PATH.'/views/scripts_base/');

define('BASE_URL','http://localhost/precisoservico/');
define('BASE_URL_VIEW_BASE', BASE_URL.'application/views/scripts_base/');
define('BASE_URL_VIEW', BASE_URL.'application/views/scripts/');

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

include APPLICATION_PATH_VIEW_BASE.'html/topo.phtml';

$application->bootstrap()
            ->run();

include APPLICATION_PATH_VIEW_BASE.'html/rodape.phtml';