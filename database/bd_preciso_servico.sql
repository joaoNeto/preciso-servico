-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Máquina: 127.0.0.1
-- Data de Criação: 22-Maio-2017 às 02:34
-- Versão do servidor: 5.5.32
-- versão do PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `bd_preciso_servico`
--
CREATE DATABASE IF NOT EXISTS `bd_preciso_servico` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bd_preciso_servico`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `buscador`
--

CREATE TABLE IF NOT EXISTS `buscador` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_user` int(10) NOT NULL,
  `link` varchar(100) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `numero` int(5) NOT NULL,
  `telefone` varchar(18) NOT NULL,
  `servico` varchar(100) NOT NULL,
  `key1` varchar(15) NOT NULL,
  `key2` varchar(15) NOT NULL,
  `key3` varchar(15) NOT NULL,
  `key4` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Extraindo dados da tabela `buscador`
--

INSERT INTO `buscador` (`id`, `id_user`, `link`, `nome`, `cidade`, `endereco`, `numero`, `telefone`, `servico`, `key1`, `key2`, `key3`, `key4`) VALUES
(10, 10, 'a', 'a', 'a', 'a', 123, '(12) 31231 - 2312', 'a', '', 'key1', 'key1', 'key1'),
(11, 20, 'a', 'a', 'a', 'a', 123, '(12) 31231 - 2312', 'a', '', 'key1', 'key1', 'key1'),
(12, 20, 'João Jose de Sousa Neto', 'João Neto', 'Feira Nova', 'Rua tres irmas', 2, '(81) 18238 - 8132', 'programador web', 'palavra chave 1', 'palavra chave 2', 'palavra chave 3', 'palavra chave 4'),
(13, 20, 'reinaldo azevedo', 'Reinaldo azevedo', 'Feira Nova', 'rua tres irmas', 2, '(81) 28318 - 2381', 'jornalista', 'key1', 'key2', 'key3', 'key4'),
(14, 20, 'sdasd', 'dasd', 'asda', 'asda', 12313, '(31) 2312', 'dsd', 'asdasdasd', 'asdasdasd', 'asdasda', 'asdasd'),
(15, 20, 'asdasd', 'asdasd', 'asdasd', 'asdasd', 21312, '(31) 23123 - 1231', 'asdasd', 'SsASASDAS', 'ASDADASD', 'ASDASD', 'ASDADASD'),
(16, 20, 'ASDAS', 'asdasdad', 'asdasd', 'asdads', 12312, '(31) 23131 - 2313', 'adasdasd', 'asdasd', 'asda', 'asdasdasd', 'asdasdasdasd'),
(17, 20, 'asdadasd', 'asdasdasd', 'dasdadasd', 'asdadasd', 12313, '(31) 23131 - 2312', 'asdasda', 'asdasd', 'asdadad', 'asdadasd', 'asdasdasd'),
(18, 20, 'Flavinho alves campos', 'Flavio pedreiro', 'Feira Nova', 'rua tres irmas', 2, '(12) 31283 - 8123', 'pedreiro', 'pedreiro', 'predio', 'pintura', 'gesso'),
(19, 20, 'asdasd', 'asdasd', 'asdadasd', 'asdasd', 12312, '(31) 23131 - 2312', 'asdasda', 'adasd', 'asdasd', 'asdasd', 'asdasds'),
(20, 20, 'asdasd', 'asdasdasd', 'asdada', 'asdasd', 12312, '(31) 23123 - 1231', 'asdasda', 'addasd', 'asdasd', 'asdasd', 'asdsda');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
